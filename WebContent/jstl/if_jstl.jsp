<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Core Tag Example</title>
</head>
<body>

	<c:set var="income" value="100"></c:set>
	<c:out value="${income}"></c:out>

	<c:choose>

		<c:when test="${ income > 50 }">
			Your income is good
		</c:when>

		<c:when test="${ income > 100 }">
			Your income is very good
		</c:when>

		<c:otherwise>
			your income is poor
		</c:otherwise>

	</c:choose>





</body>
</html>
