<html>
<head>
<title>Ajax demo with HowToDoInJava.com</title>
<script type="text/javascript">
	function ajaxAsyncRequest(reqURL) {
		//Creating a new XMLHttpRequest object
		var xmlhttp;

		xmlhttp = new XMLHttpRequest(); //for IE7+, Firefox, Chrome, Opera, Safari

		//Create a asynchronous GET request
		xmlhttp.open("GET", reqURL, true);//Asynchronously.

		//When readyState is 4 then get the server output
		xmlhttp.onreadystatechange = function() {
			//In above example, onreadystatechange is a event listener registered with XMLHttpRequest request. 
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					document.getElementById("message").innerHTML = xmlhttp.responseText;
					//alert(xmlhttp.responseText);
				} else {
					alert('Something is wrong !!');
				}
			}
		};

		xmlhttp.send(null);
	}
</script>
</head>
<body>
	<br />
	<input type="button" value="Show Server Time"
		onclick='ajaxAsyncRequest("get-current-time")' />
	<br />
	<br /> Message from server ::
	<span id="message">Hello Testing....</span>
</body>
</html>