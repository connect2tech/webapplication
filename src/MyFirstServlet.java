import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class MyFirstServlet extends HttpServlet {
	
	public void init(ServletConfig config){
		String value = config.getInitParameter("key");
		System.out.println(value);
	}
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {

		PrintWriter pw = resp.getWriter();
		
		pw.println(new Date().getTime());
		
	}
	
	
}
