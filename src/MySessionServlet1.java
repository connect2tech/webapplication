import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class MySessionServlet1 extends HttpServlet {
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {

		PrintWriter pw = resp.getWriter();
		
		HttpSession session = req.getSession();
		
		String user = req.getParameter("userName");
		
		session.setAttribute("UserValue", user);
		
		String id = session.getId();
		
		pw.println("id="+id);
		
		
	}
	
}
