import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class MySessionServlet2 extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {

		PrintWriter pw = resp.getWriter();
		
		
		HttpSession session = req.getSession();
		String id = session.getId();
		String userVal = (String)session.getAttribute("UserValue");
		
		pw.println("userVal="+userVal);
		pw.println("id="+id);
		

	}

}
