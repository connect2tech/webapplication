import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class MyDispatcherServlet extends HttpServlet {
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {
		
		System.out.println("I am get method...");
		
		
		String choice = req.getParameter("choiceFromUI");
		
		RequestDispatcher rd=null;
		
		if(choice.equals("first")){
			rd = req.getRequestDispatcher("first.html");
		}else{
			rd = req.getRequestDispatcher("second.html");
		}
		
		rd.forward(req, resp);
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {
		
		System.out.println("I am post method...");
		
		String choice = req.getParameter("choiceFromUI");
		
		RequestDispatcher rd=null;
		
		if(choice.equals("first")){
			rd = req.getRequestDispatcher("first.html");
		}else{
			rd = req.getRequestDispatcher("second.html");
		}
		
		rd.forward(req, resp);
		
	}
	
	
}
