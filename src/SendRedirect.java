import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class SendRedirect extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {

		resp.sendRedirect("https://www.google.co.in/?gfe_rd=cr&dcr=0&ei=gr-1WtqiDcTD8weJpJLQDg&gws_rd=ssl");

	}

}
