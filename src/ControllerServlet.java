import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

//import com.c2t.Employee;

public class ControllerServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {
		
		//Employee e = new Employee();
		//e.setId(100);
		//e.setName("controller");
		
		HttpSession session = req.getSession();
		//session.setAttribute("emp", e);

		RequestDispatcher rd = req.getRequestDispatcher("view2.jsp");
		rd.forward(req, resp);

	}

}
