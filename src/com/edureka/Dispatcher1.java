package com.edureka;

import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Dispatcher1 extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {

		PrintWriter pw = resp.getWriter();
		// pw.print("Welcome to Servlet");

		String val = req.getParameter("text1");
		pw.print("Welcome " + val);

		RequestDispatcher rd = null;

		if (val.equals("loggedin")) {
			rd = req.getRequestDispatcher("login.html");
		} else {
			rd = req.getRequestDispatcher("checkout.html");
		}
		rd.forward(req, resp);

	}

}
