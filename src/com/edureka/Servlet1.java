package com.edureka;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet1 extends HttpServlet{
	
	
	public void doGet(HttpServletRequest req,
            HttpServletResponse resp)
			throws ServletException,
            java.io.IOException{
		
		PrintWriter pw = resp.getWriter();
		pw.print("Welcome to Servlet");
		
	}
	
	public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
			throws ServletException,
            java.io.IOException{
		
		PrintWriter pw = resp.getWriter();
		pw.print("Welcome to Servlet, Post Method");
		
	}

}
