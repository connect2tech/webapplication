package com.edureka;

import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookiesServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {

		PrintWriter pw = resp.getWriter();

		Cookie cookie1 = new Cookie("cookie1", "Value1");
		Cookie cookie2 = new Cookie("cookie2", "Value2");

		resp.addCookie(cookie1);
		resp.addCookie(cookie2);

		pw.print("Cookie setting is done...");

		RequestDispatcher rd = req.getRequestDispatcher("cookie2.html");

		rd.forward(req, resp);

	}

}
