package com.edureka;

import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FetchCookiesForAd extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {

		PrintWriter pw = resp.getWriter();

		Cookie cookies[] = req.getCookies();

		for (int i = 0; i < cookies.length; i++) {
			String cookieName = cookies[i].getName();
			String cookieValue = cookies[i].getValue();

			pw.println("cookieName=" + cookieName);
			pw.println("cookieValue=" + cookieValue);
			pw.println("--------------------------");
		}

	}

}
