package com.c2t.pattern;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class MVCServlet extends HttpServlet {
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {

		HttpSession session = req.getSession();
		session.setAttribute("Key", "Value of the key...");
		
		
		RequestDispatcher rd = req.getRequestDispatcher("mvc2.jsp");
		rd.forward(req, resp);
		
	}
	
	
}
