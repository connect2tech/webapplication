import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class MyCookieServlet extends HttpServlet {
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, java.io.IOException {

		PrintWriter pw = resp.getWriter();
		
		Cookie cookie1 = new Cookie("Key1", "Value1");
		Cookie cookie2 = new Cookie("Key2", "Value2");
		
		resp.addCookie(cookie1);
		resp.addCookie(cookie2);
		
		pw.println("Addede cookies...");
		
		
	}
	
	
}
